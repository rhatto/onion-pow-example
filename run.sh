#!/bin/bash
set -e
TOR=${TOR:-tor}
SERVICE=./service.py
TORRC=torrc
tmux -u \
        new-session "$TOR -f $TORRC; \
	             read" \;\
        split-window -v "sleep 2; \
	                 $SERVICE $TORRC; \
			 read" \;\
        split-window -h 'while [ ! -f data/hs/hostname ]; \
                           do sleep 0.2; done; \
		         qr $(cat data/hs/hostname); \
			 cat data/hs/hostname; \
			 read' \;\
        select-pane -t 1
